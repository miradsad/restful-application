package com.mirad.restproj.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mirad.restproj.DTO.PersonDTO;
import com.mirad.restproj.entity.Person;
import com.mirad.restproj.repository.UsersRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.List;
@Tag(name = "main_methods")
@Slf4j
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class MainController {
    @Autowired
    private final UsersRepo usersRepo;
    @Operation(
            summary = "Add new person to DB",
            description = "Getting person DTO, building, then adding entity to DB"
    )
    @PostMapping
    public void addUser(@RequestBody PersonDTO personDTO){

        log.info(
                "New entity: " + usersRepo.save(
                        Person.builder()
                            .age(personDTO.getAge())
                            .name(personDTO.getName())
                            .height(personDTO.getHeight())
                            .build())
                        );
    }
    @SneakyThrows
    @GetMapping
    public List<Person> getAll() throws JsonProcessingException {
        return usersRepo.findAll();
    }
    @GetMapping("/{id}")
    public Person getUser(@RequestParam int id){
        return usersRepo.findById(id).orElseThrow();
    }
    @DeleteMapping("/{id}")
    public void deleteUser(@RequestParam int id){
        usersRepo.deleteById(id);
    }
    @PutMapping("/{id}")
    public String changeUser(@RequestBody Person person){
        if (!usersRepo.existsById(person.getId())){
            return "Error: Entity not found";
        }
        return usersRepo.save(person).toString();
    }

    @Autowired
    Environment env;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("driverClassName"));
        dataSource.setUrl(env.getProperty("url"));
        dataSource.setUsername(env.getProperty("user"));
        dataSource.setPassword(env.getProperty("password"));
        return dataSource;
    }

}
