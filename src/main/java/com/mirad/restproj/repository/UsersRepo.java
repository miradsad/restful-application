package com.mirad.restproj.repository;

import com.mirad.restproj.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepo extends JpaRepository<Person, Integer> {

}
